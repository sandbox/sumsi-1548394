<?php

/**
 * Implements hook_default_services_endpoint().
 */
function tmgmt_service_default_services_endpoint() {
  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'translator';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'translation-service';
  $endpoint->debug = 0;
  $endpoint->authentication = array(
    'tmgmt_auth' => 'tmgmt_auth',
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'bencode' => TRUE,
      'json' => TRUE,
      'php' => TRUE,
      'rss' => TRUE,
      'xml' => TRUE,
      'jsonp' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/vnd.php.serialized' => TRUE,
      'application/x-www-form-urlencoded' => TRUE,
      'multipart/form-data' => TRUE,
    ),
  );
  $endpoint->resources = array(
    'drupal-translator-item' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'drupal-translator-job' => array(
      'actions' => array(
        'order' => array(
          'enabled' => '1',
        ),
        'cancel' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'drupal-translator-languages' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );

  return array($endpoint);
}