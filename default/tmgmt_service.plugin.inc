<?php

/**
 * Controller class for the drupal translation service resources.
 */
class TMGMTDrupalResourcesController extends TMGMTDefaultResourcesController {

  /**
   * Overrides TMGMTDefaultResourcesController::getResponsibleTranslator().
   */
  public function getResponsibleTranslator() {
    $translator = variable_get('tmgmt_service_translator');

    if ($translator && $translator != '_none') {
      return tmgmt_translator_load($translator);
    }
  }

}
