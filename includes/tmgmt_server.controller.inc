<?php

/**
 * Entity controller for the remote source entity.
 */
class TMGMTRemoteSourceController extends EntityAPIController {

  /**
   * Overrides EntityAPIController::save().
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    // Update the 'changed' timestamp.
    $entity->changed = REQUEST_TIME;

    parent::save($entity, $transaction);
  }

}