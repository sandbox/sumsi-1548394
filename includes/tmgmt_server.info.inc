<?php

/**
 * @file
 * Contains the metadata controller classes for the Translation Management Tool
 * entities.
 */

/**
 * Metadata controller for the remote source entity.
 */
class TMGMTRemoteSourceMetadataController extends EntityDefaultMetadataController {

  /**
   * Overrides EntityDefaultMetadataController::entityPropertyInfo().
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $info = _tmgmt_server_override_property_description($info, $this->type);
    $properties = &$info[$this->type]['properties'];

    // Make the created and changed property appear as date.
    $properties['changed']['type'] = $properties['created']['type'] = 'date';

    // Use the defined entity label callback instead of the custom label directly.
    $properties['label']['getter callback'] = 'entity_class_label';

    // Add the options list for the available languages.
    $properties['target_language']['options list'] = $properties['source_language']['options list'] = 'entity_metadata_language_list';

    // Add the options list for the defined state constants.
    $properties['state']['options list'] = 'tmgmt_server_remote_source_states';

    // Add the options list for the available resources plugin names.
    $properties['plugin']['options list'] = 'tmgmt_server_resources_plugin_names';

    // Link the author property to the corresponding user entity.
    $properties['owner'] = array(
      'label' => t('Owner'),
      'type' => 'user',
      'description' => t('The client that the remote source belongs to.'),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer tmgmt',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    return $info;
  }

}

/**
 * Populates all entity property descriptions based on the schema definition.
 *
 * @param $info
 *  Entity propety info array.
 *
 * @return
 *   The altered entity properties array.
 */
function _tmgmt_server_override_property_description($info, $entity_type) {
  // Load tmgmt_server.install so we can access the schema.
  module_load_install('tmgmt_server');

  $entity_info = entity_get_info($entity_type);
  $schema = tmgmt_server_schema();
  $fields = $schema[$entity_info['base table']]['fields'];

  foreach ($info[$entity_type]['properties'] as $name => &$property_info) {
    if (isset($fields[$name]['description'])) {
      $properties[$name]['description'] = $fields[$name]['description'];
    }
  }

  return $info;
}
