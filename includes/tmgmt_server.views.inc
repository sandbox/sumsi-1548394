<?php

/**
 * @file
 * Contains Views controllers for the Translation Management Server module.
 */

/**
 * Views controller class for the remote source entity.
 */
class TMGMTRemoteSourceViewsController extends EntityDefaultViewsController {

  /**
   * Overrides EntityDefaultViewsController::views_data().
   */
  public function views_data() {
    $data = parent::views_data();

    return $data;
  }

}