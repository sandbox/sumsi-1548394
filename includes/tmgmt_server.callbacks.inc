<?php

/**
 * Relay endpoint callback that forwards the function call to the configured
 * method on the corresponding resources controller.
 */
function tmgmt_server_resources_controller_callback() {
  $args = func_get_args();

  // Remove the last two argument (the plugin type and the method name).
  list($plugin, $method) = array_splice($args, -2);

  // Retrieve the resource controller.
  $controller = tmgmt_server_resources_controller($plugin);

  // Invoke the callback method on the controller.
  return call_user_func_array(array($controller, $method), $args);
}

/**
 * Relay endpoint callback that forwards the function call to the configured
 * method on the corresponding resources controller.
 */
function tmgmt_server_resources_controller_access_callback() {
  $args = func_get_args();

  // Remove the first two argument (the plugin type and the method name).
  list($plugin, $method) = array_splice($args, 0, 2);

  // Retrieve the resource controller.
  $controller = tmgmt_server_resources_controller($plugin);

  // Invoke the callback method on the controller.
  return call_user_func_array(array($controller, $method), $args);
}