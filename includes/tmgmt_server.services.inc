<?php

/**
 * Contains the interface definition for translation service resources
 * controllers as well as a default (base) implementation.
 */

/**
 * Interface for translation service resources.
 */
interface TMGMTResourcesController extends TMGMTPluginBaseInterface {

  /**
   * Implements hook_services_resources().
   */
  public function hook_services_resources();

}

/**
 * Default implementation for translation service resources.
 */
class TMGMTDefaultResourcesController extends TMGMTPluginBase implements TMGMTResourcesController {

  /**
   * Implements TMGMTResourcesController::hook_services_resources().
   */
  public function hook_services_resources() {
    $resources[$this->pluginType . '-translator-item']['operations']['retrieve'] = array(
      'help' => t('Pulls a finished translation.'),
      'access arguments append' => TRUE,
      'args' => array(
        array(
          'name' => 'id',
          'optional' => FALSE,
          'source' => array('path' => 0),
          'type' => 'int',
          'description' => 'The primary identifier of the remote source entity that should be pulled.',
        ),
      ),
    );

    $resources[$this->pluginType . '-translator-job']['actions']['order'] = array(
      'help' => t('Requests a translation.'),
      'access arguments' => array('use translation services'),
      'args' => array(
        array(
          'name' => 'data',
          'optional' => FALSE,
          'source' => array('data' => 'data'),
          'type' => 'string',
          'description' => 'The data array that should be translated.',
        ),
      ),
    );

    $resources[$this->pluginType . '-translator-job']['actions']['cancel'] = array(
      'help' => t('Cancels a translation order.'),
      'access arguments append' => TRUE,
      'args' => array(
        array(
          'name' => 'id',
          'optional' => FALSE,
          'source' => array('data' => 'id'),
          'type' => 'int',
          'description' => 'The primary identifier of the remote job that should be pulled.',
        ),
      ),
    );

    $resources[$this->pluginType . '-translator-languages']['operations']['index'] = array(
      'help' => t('Retrieves the target languages that are supported by the translation server.'),
      'access arguments' => array('use translation services'),
      'args' => array(
        array(
          'name' => 'language',
          'optional' => TRUE,
          'source' => array('param' => 'language'),
          'type' => 'string',
          'description' => 'The source language for which to retrieve the supported target language.',
        ),
      ),
    );

    // Use controller callback methods.
    _tmgmt_server_resources_use_controller_callback($resources[$this->pluginType . '-translator-item']['operations']['retrieve'], $this->pluginType, 'pullTranslation');
    _tmgmt_server_resources_use_controller_callback($resources[$this->pluginType . '-translator-job']['actions']['order'], $this->pluginType, 'processTranslationOrder');
    _tmgmt_server_resources_use_controller_callback($resources[$this->pluginType . '-translator-job']['actions']['cancel'], $this->pluginType, 'cancelTranslation');
    _tmgmt_server_resources_use_controller_callback($resources[$this->pluginType . '-translator-languages']['operations']['index'], $this->pluginType, 'getLanguagesIndex');

    // Use controller access callback methods.
    _tmgmt_server_resources_use_controller_access_callback($resources[$this->pluginType . '-translator-item']['operations']['retrieve'], $this->pluginType, 'pullAccess');
    _tmgmt_server_resources_use_controller_access_callback($resources[$this->pluginType . '-translator-job']['actions']['cancel'], $this->pluginType, 'cancelAccess');

    return $resources;
  }

  /**
   * Services callback for building an index of available language combinations.
   *
   * @param $language
   *   The source language that the index should be built for.
   */
  public function getLanguagesIndex($language) {
    // Retrieve the translator that is in charge of this translation order.
    if ($translator = $this->getResponsibleTranslator()) {
      return $translator->getSupportedTargetLanguages($language);
    }

    return array();
  }

  /**
   * Cancels a translation job.
   */
  public function cancelTranslation($id) {
    $job = tmgmt_job_load($id);
    // Cancel the translation job.
    $job->cancelled();
  }

  /**
   * Services callback for processing a translation order.
   *
   * @param $data
   *   A JSON encoded array with the following properties:
   *     - from: The language code of the source language that the data should
   *       be translated from.
   *     - to: The language code of the target language that the data should be
   *       translated into. where each array item is an independent translation
   *     - items: An array of translation order items with the following
   *       properties for each item:
   *         - data: A flattened array of data items that should be translated.
   *           This includes the strings as well as the metadata for each the
   *           data item as defined for tmgmt_flatten_data() and
   *           tmgmt_unflatten_data().
   *         - callback: (Optional) A callback URL that should be invoked when
   *           the translation for the item is finished.
   *         - reference: (Optional) A custom remote reference that will be used
   *           in conjunction with the given callback URL when notifying the
   *           remote client about the finished translation.
   */
  public function processTranslationOrder($data) {
    // Unwrap the data array.
    $data = json_decode($data, TRUE);

    // Give modules a chance to alter the input data.
    drupal_alter('tmgmt_server_translation_order', $data, $this->pluginType);

    // Throw an exception if the translation order is empty.
    if (empty($data['items'])) {
      throw new TMGMTServerTranslationOrderException('The given translation order did not contain any translatable items.');
    }

    // Retrieve the translator that is in charge of this translation order.
    $translator = $this->getResponsibleTranslator();

    // Throw an exception if the translation order can not be processed.
    if (!$this->canProcessTranslationOrder($data, $translator)) {
      throw new TMGMTServerTranslationOrderException('The given translation order can not be processed by the server.');
    }

    // Use a db transaction so we can rollback any changes in case of an error
    // when processing the remote source entities.
    $transaction = db_transaction();

    try {
      // Iterate over the provided translation items and create a source object
      // for each of them.
      $sources = array();
      foreach ($data['items'] as $key => $item) {
        $values = array('reference' => $key);

        if (!empty($item['callback'])) {
          $values['callback'] = $item['callback'];
        }

        if (!empty($item['label'])) {
          $values['label'] = $item['label'];
        }

        // Create and save the remote source entity.
        $sources[$key] = tmgmt_server_remote_source_create($item['data'], $item['from'], $item['to'], $GLOBALS['user']->uid, $this->pluginType, $values);
        $sources[$key]->save();
      }

      // Ignore slave server temporarily.
      db_ignore_slave();

      // Allow implementing plugins to further process the translation order by
      // processing the generated remote sources.
      $reference = NULL;
      if ($translator && ($reference = $this->executeTranslationOrder($translator, $sources, $data)) === FALSE) {
        // Something went wrong while trying to execute the translation order.
        throw new TMGMTServerTranslationOrderException('An error occured while processing your translation order.');
      }

      // Build the response for the translation order request. In most cases
      // this should be an array with the inbound translation order item keys
      // as keys and the internal reference or finished translation data as
      // values.
      return $this->getTranslationOrderResponse($sources, $reference);
    }
    catch (Exception $e) {
      $transaction->rollback();

      // Forward the exception so that the services module can output something
      // to the client if it is a services exception.
      throw $e;
    }
  }

  /**
   * Validate that the passed combination of translator and translation data can
   * processed by the plugin.
   *
   * @param $data
   *   The input data structure as passed by processTranslationOrder().
   * @param $translator
   *   (Optional) The translator that is supposed to handle the given
   *   translation order. In some cases we don't know which translator is going
   *   to handle a translation order (e.g. in cases where the server carrier
   *   wants to manually assign a translator and thereby act as a relay).
   *
   * @see processTranslationOrder()
   */
  public function canProcessTranslationOrder($data, $translator = NULL) {
    // By default we can only process translation orders that are supported by
    // the translator that is responsible for the translation.
    if ($translator && $translator->getSupportedTargetLanguages($data['from'], $data['to'])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Executes a translation order.
   */
  public function executeTranslationOrder($translator, $sources, $data) {
    // Create a job for the given remote sources.
    $job = tmgmt_job_create($data['from'], $data['to'], $GLOBALS['user']->uid, array(
      'translator' => $translator->name,
    ));

    // Add the remote source entities to the previously created job.
    foreach ($sources as $source) {
      $job->addItem('remote', 'remote', $source->rsid);
    }

    // Request the translation for this job.
    if ($job->requestTranslation() === FALSE) {
      return FALSE;
    }

    return $job->tjid;
  }

  /**
   * Builds the response for a translation order.
   */
  public function getTranslationOrderResponse($sources, $reference = NULL) {
    $output = isset($reference) ? array('reference' => $reference) : array();

    foreach ($sources as $key => $source) {
      $source = entity_load_unchanged('tmgmt_server_remote_source', $source->rsid);

      // Check if the translation is already finished and accepted (e.g. in
      // case of fully automated translations with skipped review) and directly
      // return the translated data in that case.
      if ($source->state == TMGMT_SERVER_REMOTE_SOURCE_TRANSLATED) {
        $output['data'][$key] = $source->data;
      }
    }

    // Give modules a chance to alter the response.
    drupal_alter('tmgmt_server_translation_order_response', $output, $sources);

    return $output;
  }

  /**
   * Pulls a translation from the server.
   */
  public function pullTranslation($id) {
    return entity_load_single('tmgmt_server_remote_source', $id)->data;
  }

  /**
   * Access callback for pulling a translation.
   */
  public function pullAccess($id) {
    if ($source = tmgmt_server_remote_source_load($id)) {
      if ($source->uid == $GLOBALS['user']->uid && $source->state == TMGMT_SERVER_REMOTE_SOURCE_TRANSLATED) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Access callback for pulling a translation.
   */
  public function cancelAccess($id) {
    if ($job = tmgmt_job_load($id)) {
      if ($job->uid == $GLOBALS['user']->uid && $job->isActive()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Retrieves the responsible translator.
   */
  public function getResponsibleTranslator() {
    return FALSE;
  }

}

/**
 * Exception class for translation order exceptions.
 */
class TMGMTServerTranslationOrderException extends ServicesException {

}
