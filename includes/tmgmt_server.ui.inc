<?php

/**
 * UI Controller for remote translation sources.
 */
class TMGMTRemoteSourceUIController extends TMGMTDefaultSourceUIController {

  /**
   * Overrides TMGMTDefaultSourceUIController::hook_menu().
   */
  public function hook_menu() {
    // The remote source overview is a View using Views Bulk Operations.
    // Therefore we don't need to provide any menu items.
    return array();
  }

  /**
   * Overrides TMGMTDefaultSourceUIController::hook_form().
   */
  public function hook_forms() {
    // The remote source overview is a View using Views Bulk Operations.
    // Therefore we don't need to provide any forms.
    return array();
  }

  /**
   * Overrides TMGMTDefaultSourceUIController::hook_views_default_views().
   */
  public function hook_views_default_views() {
    return _tmgmt_load_exports('tmgmt_server', 'views', 'view.inc', 'view');
  }

}
