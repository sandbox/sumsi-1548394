<?php

/**
 * Entity object for the remote source entity.
 */
class TMGMTRemoteSource extends Entity {

  /**
   * Overrides Entity::__construct().
   */
  function __construct(array $values = array()) {
    parent::__construct($values, 'tmgmt_server_remote_source');

    if (empty($this->rsid)) {
      $this->created = REQUEST_TIME;
    }
  }

}