<?php

/**
 * Entity controller for the client entity.
 */
class TMGMTClientController extends EntityAPIController {

  /**
   * Overrides EntityAPIController::save().
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();
    try {
      // Load the stored entity, if any.
      if (!empty($entity->{$this->idKey}) && !isset($entity->original)) {
        // In order to properly work in case of name changes, load the original
        // entity using the id key if it is available.
        $entity->original = entity_load_unchanged($this->entityType, $entity->{$this->idKey});
      }

      if (empty($entity->cid)) {
        $unique = FALSE;

        while (!$unique) {
          $public_key = user_password(64);

          // Make sure that the public key is unique.
          $query = new EntityFieldQuery();
          $result = $query->entityCondition('entity_type', 'tmgmt_auth_client')
            ->propertyCondition('public_key', $public_key)
            ->execute();

          $unique = empty($result['tmgmt_client']);
        }

        // Add the public and private API keys.
        $entity->public_key = $public_key;
        $entity->private_key = user_password(64);
      }
      else {
        // Override the public and private API key properties if the entity
        // already exists in the database. We don't want to change those
        // values... ever.
        $entity->public_key = $entity->original->public_key;
        $entity->private_key = $entity->original->private_key;
      }

      // Update the 'changed' timestamp.
      $entity->changed = REQUEST_TIME;

      parent::save($entity, $transaction);
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }

}
