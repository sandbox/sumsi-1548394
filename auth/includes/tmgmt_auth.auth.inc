<?php

/**
 * @file
 * Contains the services authentication callback and helper functions.
 */

/**
 * Authentication callback for the translation client authentication.
 */
function tmgmt_auth_authentication_callback($settings, $method, $args) {
  try {
    // Check if all required authentication information is available.
    if (!isset($_REQUEST['public_key']) || !isset($_REQUEST['signed_key']) || !isset($_REQUEST['timestamp']) || (float) $_REQUEST['timestamp'] != $_REQUEST['timestamp']) {
      throw new TMGMTServerAuthenticationMalformedException('Relevant information for authentication missing or malformed.');
    }

    // After 3 failed authentication attempts we ban the public key for any
    // further use with this authentication method for the next hour.
    if (!flood_is_allowed('tmgmt_server_auth_fail', 3, 3600, $_REQUEST['public_key'])) {
      throw new TMGMTServerAuthenticationBlockedException('The given public key has been blocked for 60 minutes after 3 failed authentication attempts.');
    }

    // Check if the given public key can be related to a client entity.
    if (!$client = tmgmt_auth_client_load_by_public_key($_REQUEST['public_key'])) {
      throw new TMGMTServerAuthenticationInvalidException('The given authentication information is invalid.');
    }

    // Add the current client entity to the server info.
    services_set_server_info('client', $client);

    // Check if the timestamp sent for authentification is higher than the one
    // used with the most recent request.
    if (!empty($client->last_request) && (float) $_REQUEST['timestamp'] <= $client->last_request) {
      // Register the failed authentication attempt.
      flood_register_event('tmgmt_server_auth_fail', 60, $_REQUEST['public_key']);

      throw new TMGMTServerAuthenticationInvalidException('The given authentication information is invalid.');
    }

    // Check if the provided signed key is valid.
    if ($_REQUEST['signed_key'] !== hash_hmac('sha256', $_REQUEST['timestamp'], $client->private_key)) {
      // Register the failed authentication attempt.
      flood_register_event('tmgmt_server_auth_fail', 60, $_REQUEST['public_key']);

      throw new TMGMTServerAuthenticationInvalidException('The given authentication information is invalid.');
    }

    // Update the 'last_request' property on the client entity.
    $client->last_request = $_REQUEST['timestamp'];
    $client->save();

    // Since we got up to this point we can clear the flood event.
    flood_clear_event('tmgmt_server_auth_fail', $_REQUEST['public_key']);

    // Now that the client entity has been authenticated we can safely load
    // and inject the related user.
    $GLOBALS['user'] = user_load($client->uid);
  }
  catch (TMGMTServerAuthenticationException $e) {
    drupal_add_http_header('WWW-Authenticate', sprintf('TMGMTServerAuthentication realm="%s"', url('', array('absolute' => TRUE))));

    return $e->getMessage();
  }
}

/**
 * Exception class for server authentication exceptions generated by the
 * Translation Server authentication mechanism.
 */
class TMGMTServerAuthenticationException extends Exception {

}

/**
 * Exception class for malformed authentication information.
 */
class TMGMTServerAuthenticationMalformedException extends TMGMTServerAuthenticationException {

}

/**
 * Exception class for invalid authentication requests.
 */
class TMGMTServerAuthenticationInvalidException extends TMGMTServerAuthenticationException {

}

/**
 * Exception class for when a client is blocked.
 */
class TMGMTServerAuthenticationBlockedException extends TMGMTServerAuthenticationException {

}