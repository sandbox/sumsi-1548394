<?php

/**
 * @file
 * Contains Views controllers for the Translation Management Server module.
 */

/**
 * Views controller class for the client entity.
 */
class TMGMTClientViewsController extends EntityDefaultViewsController {

  /**
   * Overrides EntityDefaultViewsController::views_data().
   */
  public function views_data() {
    $data = parent::views_data();

    return $data;
  }

}
