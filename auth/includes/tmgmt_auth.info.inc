<?php

/**
 * @file
 * Contains the metadata controller classes for the Translation Management Tool
 * entities.
 */

/**
 * Metadata controller for the client entity.
 */
class TMGMTClientMetadataController extends EntityDefaultMetadataController {

  /**
   * Overrides EntityDefaultMetadataController::entityPropertyInfo().
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $info = _tmgmt_auth_override_property_description($info, $this->type);
    $properties = &$info[$this->type]['properties'];

    // Make the created and changed property appear as date.
    $properties['changed']['type'] = $properties['created']['type'] = 'date';

    // Use the defined entity label callback instead of the custom label directly.
    $properties['label']['getter callback'] = 'entity_class_label';

    // Link the author property to the corresponding user entity.
    $properties['owner'] = array(
      'label' => t('Owner'),
      'type' => 'user',
      'description' => t('The owner of the client entity.'),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer translation clients',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    return $info;
  }

}

/**
 * Populates all entity property descriptions based on the schema definition.
 *
 * @param $info
 *  Entity propety info array.
 *
 * @return
 *   The altered entity properties array.
 */
function _tmgmt_auth_override_property_description($info, $entity_type) {
  // Load tmgmt_auth.install so we can access the schema.
  module_load_install('tmgmt_auth');

  $entity_info = entity_get_info($entity_type);
  $schema = tmgmt_auth_schema();
  $fields = $schema[$entity_info['base table']]['fields'];

  foreach ($info[$entity_type]['properties'] as $name => &$property_info) {
    if (isset($fields[$name]['description'])) {
      $properties[$name]['description'] = $fields[$name]['description'];
    }
  }

  return $info;
}
