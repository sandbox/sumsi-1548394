<?php

/**
 * Entity object for the client entity.
 */
class TMGMTClient extends Entity {

  /**
   * The label of the client entity.
   *
   * @var string
   */
  public $label;

  /**
   * A string that describes the client entity. Can be used by users to help
   * maintaining a list of multiple client entities.
   *
   * @var string
   */
  public $description;

  /**
   * The public API key of the client entity.
   *
   * @var string
   */
  public $public_key;

  /**
   * The private API key of the client entity. Used for signing the public key
   * when communicating via the web services interface.
   *
   * @var string
   */
  public $private_key;

  /**
   * Overrides Entity::__construct().
   */
  function __construct(array $values = array()) {
    parent::__construct($values, 'tmgmt_auth_client');

    if (empty($this->cid)) {
      $this->created = REQUEST_TIME;
    }
  }

}
