<?php

/**
 * Page callback for the client entity listing.
 */
function tmgmt_auth_client_profile_page($user) {
  $output = array();
  $original = drupal_get_title();
  $entity = entity_create('tmgmt_auth_client', array('uid' => $user->uid));

  // Override the title that was set by the Entity API module.
  drupal_set_title($original, PASS_THROUGH);

  // Try to prepend the client view to the output.
  $output['clients']['#markup'] = views_embed_view('tmgmt_auth_clients');

  // Check if we can append the 'add new client entity' form.
  if (($user->uid == $GLOBALS['user']->uid && user_access('manage own translation clients')) || user_access('administer translation server')) {
    $form = entity_form('tmgmt_auth_client', $entity);

    // Append the entity form.
    $output['form'] = array(
      '#theme' => 'fieldset',
      '#title' => t('Generate a new client'),
      '#description' => t('You can use this form to add a new client to your profile.'),
      '#children' => drupal_render($form),
    );
  }

  return $output;
}
