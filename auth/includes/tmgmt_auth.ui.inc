<?php

/**
 * Administrative interface for translation client entities.
 */
class TMGMTClientUIController extends EntityDefaultUIController {

  /**
   * Overrides EntityDefaultUIController::hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();

    // Turn the main menu callback into a local task and shorten the title.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    $items[$this->path]['title'] = 'Clients';
    $items[$this->path]['weight'] = 10;
    return $items;
  }

  /**
   * Overrides EntityDefaultUIController::overviewTable().
   *
   * @todo Create a Views / Views Bulk Operations based Entity API UI Controller
   * and make use of that instead of custom code.
   */
  public function overviewTable($conditions = array()) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // Add all conditions to query.
    foreach ($conditions as $key => $value) {
      $query->propertyCondition($key, $value);
    }

    if ($this->overviewPagerLimit) {
      $query->pager($this->overviewPagerLimit);
    }

    $results = $query->execute();

    $ids = isset($results[$this->entityType]) ? array_keys($results[$this->entityType]) : array();
    $entities = $ids ? entity_load($this->entityType, $ids) : array();
    ksort($entities);

    $rows = array();
    foreach ($entities as $entity) {
      $user = theme('username', array('account' => user_load($entity->uid)));
      $rows[] = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity, array($user, $entity->public_key, $entity->private_key));
    }

    $render = array(
      '#theme' => 'table',
      '#header' => $this->overviewTableHeaders($conditions, $rows, array(t('Owner'), t('Public key'), t('Private key'))),
      '#rows' => $rows,
      '#empty' => t('None.'),
    );

    return $render;
  }
}

/**
 * Entity form callback for the Client entity.
 */
function tmgmt_auth_client_form($form, &$form_state, $entity) {
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The label of the client entity.'),
    '#default_value' => $entity->label,
    '#maxlength' => 32,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('A short description for the client entity.'),
    '#default_value' => $entity->description,
    '#maxlength' => 256,
  );

  if ((!isset($entity->cid) && isset($entity->uid)) || !user_access('administer translation clients')) {
    $form['uid'] = array(
      '#type' => 'value',
      '#value' => !empty($entity->uid) ? $entity->uid : $GLOBALS['user']->uid,
      '#required' => TRUE,
    );
  }
  else {
    $form['uid'] = array(
      '#type' => 'textfield',
      '#title' => t('Owner'),
      '#autocomplete_path' => 'user/autocomplete',
      '#size' => '6',
      '#maxlength' => '60',
      '#description' => t('The owner of the client entity.'),
      '#required' => TRUE,
      '#element_validate' => array('tmgmt_auth_client_form_element_validate_uid')
    );

    if (!empty($entity->uid) && $user = user_load($entity->uid)) {
      $form['uid']['#default_value'] = $user->name;
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => !empty($entity->cid) ? t('Save client') : t('Add client'),
  );

  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#access' => !empty($entity->cid),
  );

  return $form;
}

/**
 * Validates settings form for node_assign_owner_action().
 */
function tmgmt_auth_client_form_element_validate_uid($element, &$form_state) {
  $value = $element['#value'];
  if (!$user = user_load_by_name($value)) {
    form_set_error('uid', t('Enter a valid username.'));
  }
  else {
    // Username can change, so we need to store the ID, not the username.
    form_set_value($element, $user->uid, $form_state);
  }
}

/**
 * Form submit handler for the Client entity form.
 */
function tmgmt_auth_client_form_submit($form, &$form_state) {
  $entity = $form_state['tmgmt_auth_client'];

  // Process the submitted form values and save the entity.
  entity_form_submit_build_entity('tmgmt_auth_client', $entity, $form, $form_state);
  $entity->save();

  // Output a success message to the user.
  drupal_set_message(t('The client entity has been saved.'));
}
