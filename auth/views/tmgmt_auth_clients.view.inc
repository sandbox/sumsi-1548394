<?php

$view = new view();
$view->name = 'tmgmt_auth_clients';
$view->description = 'List of client entities.';
$view->tag = 'default';
$view->base_table = 'tmgmt_auth_client';
$view->human_name = 'Client entities';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['row_plugin'] = 'fields';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'You do not have any client entities attached to your account.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Field: Label */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'tmgmt_auth_client';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['ui_name'] = 'Label';
$handler->display->display_options['fields']['label']['label'] = '';
$handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
/* Field: Description */
$handler->display->display_options['fields']['description']['id'] = 'description';
$handler->display->display_options['fields']['description']['table'] = 'tmgmtauth_client';
$handler->display->display_options['fields']['description']['field'] = 'description';
$handler->display->display_options['fields']['description']['ui_name'] = 'Description';
$handler->display->display_options['fields']['description']['label'] = '';
$handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
/* Field: Public key */
$handler->display->display_options['fields']['public_key']['id'] = 'public_key';
$handler->display->display_options['fields']['public_key']['table'] = 'tmgmt_auth_client';
$handler->display->display_options['fields']['public_key']['field'] = 'public_key';
$handler->display->display_options['fields']['public_key']['ui_name'] = 'Public key';
$handler->display->display_options['fields']['public_key']['label'] = 'Public key';
/* Field: Private key */
$handler->display->display_options['fields']['private_key']['id'] = 'private_key';
$handler->display->display_options['fields']['private_key']['table'] = 'tmgmt_auth_client';
$handler->display->display_options['fields']['private_key']['field'] = 'private_key';
$handler->display->display_options['fields']['private_key']['ui_name'] = 'Private key';
$handler->display->display_options['fields']['private_key']['label'] = 'Private key';
/* Contextual filter: Translation Management Client: Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'tmgmt_auth_client';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
$handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$translatables['tmgmt_auth_clients'] = array(
  t('Master'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('You do not have any client entities attached to your account.'),
  t('Private key'),
  t('Public key'),
  t('All'),
);
